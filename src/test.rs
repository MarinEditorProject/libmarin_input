use nom::{
    IResult,
    bytes::complete::{
        take_while,
        tag,
    },
};
use crate::Input as CustomInput;

type Span<'l> = CustomInput<'l, &'l str>;
type SResult<'l> = IResult<Span<'l>, Span<'l>>;

#[test]
fn test_line() {
    let line_span = Span::new("test_line.txt", "

    Three new lines");

    let skip_lines: SResult = take_while(|chr: char| {
        if chr.is_whitespace() {
            true
        } else {
            false
        }
    })(line_span);

    assert_eq!(skip_lines.unwrap().0.get_line(), 3);
}

#[test]
fn test_column() {
    let column_span = Span::new("test_column.txt", "Columns and big pig...");
    let columns: SResult = tag("Columns")(column_span);

    assert_eq!(columns.unwrap().0.get_column(), 8);
}
