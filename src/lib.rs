#[cfg(test)]
mod test;

use std::{
  iter::{
    Copied,
    Enumerate,
  },
  str::{
    Chars,
    CharIndices,
    FromStr,
  },
  ops::{
    Deref,
    Range,
    RangeTo,
    RangeFrom,
    RangeFull,
  },
  slice::{
    Iter,
    from_raw_parts as slice_from_raw_parts,
  },
  fmt::{
    Display,
    Formatter,
    Result as FmtResult,
  },
};

use nom::{
  AsBytes,
  Compare,
  ExtendInto,
  FindSubstring,
  FindToken,
  InputIter,
  InputLength,
  InputTake,
  InputTakeAtPosition,
  Offset,
  ParseTo,
  Slice,

  IResult,
  CompareResult,
  Needed,
  Err,
  error::{
    ErrorKind,
    ParseError,
  },
};

use memchr::{
  Memchr,
  memrchr,
};

#[derive(Debug, Clone)]
pub struct Input<'l, C> {
  line: usize,
  column: usize,
  offset: usize,
  filename: &'l str,
  content: C,
}

impl<'l, C: Display> Display for Input<'l, C> {
  fn fmt(&self, f: &mut Formatter) -> FmtResult {
    write!(f, "{}", self.content)
  }
}

impl<'l, C: AsBytes> Input<'l, C> {
  pub fn new(filename: &'l str, content: C) -> Self {
    Self {
      line: 1,
      column: 1,
      offset: 0,
      filename,
      content,
    }
  }

  pub fn get_line(&self) -> usize {
    self.line
  }

  pub fn get_column(&self) -> usize {
    self.column
  }

  pub fn get_offset(&self) -> usize {
    self.offset
  }

  pub fn get_filename(&self) -> &'l str {
    self.filename
  }

  pub fn get_content(&self) -> &C {
    &self.content
  }

  fn get_unoffsetted_slice(&self) -> &[u8] {
    let self_bytes = self.content.as_bytes();
    let self_ptr = self_bytes.as_ptr();
    unsafe {
      assert!(
        self.offset <= isize::max_value() as usize,
        "offset is too big"
      );
      let orig_input_ptr = self_ptr.offset(-(self.offset as isize));
      slice_from_raw_parts(
        orig_input_ptr,
        self.offset + self_bytes.len(),
      )
    }
  }
}

impl<'l, C> Deref for Input<'l, C> {
  type Target = C;
  fn deref(&self) -> &Self::Target {
    &self.content
  }
}

impl<'l, C: AsBytes> From<C> for Input<'l, C> {
  fn from(i: C) -> Self {
    Input::new("", i)
  }
}

impl<'l, C: AsBytes> AsBytes for Input<'l, C> {
  fn as_bytes(&self) -> &[u8] {
    self.content.as_bytes()
  }
}

impl<'l, C: Compare<B>, B: Into<Input<'l, B>>> Compare<B> for Input<'l, C> {
  fn compare(&self, t: B) -> CompareResult {
    self.content.compare(t.into().content)
  }

  fn compare_no_case(&self, t: B) -> CompareResult {
    self.content.compare_no_case(t.into().content)
  }
}

macro_rules! impl_extend_into {
  ($content_type:ty, $item:ty, $extender:ty) => {
    impl<'l> ExtendInto for Input<'l, $content_type> {
      type Item = $item;
      type Extender = $extender;

      fn new_builder(&self) -> Self::Extender {
        self.content.new_builder()
      }

      fn extend_into(&self, acc: &mut Self::Extender) {
        self.content.extend_into(acc)
      }
    }
  };
}

impl_extend_into!(&'l str, char, String);
impl_extend_into!(&'l [u8], u8, Vec<u8>);

impl<'l, C> FindSubstring<&'l str> for Input<'l, C>
where
  C: FindSubstring<&'l str>,
{
  fn find_substring(&self, substr: &'l str) -> Option<usize> {
    self.content.find_substring(substr)
  }
}

impl<'l, C: FindToken<Token>, Token> FindToken<Token> for Input<'l, C> {
  fn find_token(&self, token: Token) -> bool {
    self.content.find_token(token)
  }
}

macro_rules! impl_input_iter {
  ($content_type:ty, $item:ty, $iter:ty, $iter_elem:ty) => {
    impl<'l> InputIter for Input<'l, $content_type> {
      type Item = $item;
      type Iter = $iter;
      type IterElem = $iter_elem;

      fn iter_indices(&self) -> Self::Iter {
        self.content.iter_indices()
      }

      fn iter_elements(&self) -> Self::IterElem {
        self.content.iter_elements()
      }

      fn position<P>(&self, pred: P) -> Option<usize>
      where
        P: Fn(Self::Item) -> bool,
      {
        self.content.position(pred)
      }

      fn slice_index(&self, count: usize) -> Result<usize, Needed> {
        self.content.slice_index(count)
      }
    }
  };
}

impl_input_iter!(
  &'l str,
  char,
  CharIndices<'l>,
  Chars<'l>
);

impl_input_iter!(
  &'l [u8],
  u8,
  Enumerate<Self::IterElem>,
  Copied<Iter<'l, Self::Item>>
);

impl<'l, C: InputLength> InputLength for Input<'l, C> {
  fn input_len(&self) -> usize {
    self.content.input_len()
  }
}

impl<'l, C> InputTake for Input<'l, C>
where
  Self: Slice<RangeFrom<usize>> + Slice<RangeTo<usize>>,
{
  fn take(&self, count: usize) -> Self {
    self.slice(..count)
  }

  fn take_split(&self, count: usize) -> (Self, Self) {
    (self.slice(count..), self.slice(..count))
  }
}

impl<'l, C> InputTakeAtPosition for Input<'l, C>
where
  C: InputTakeAtPosition + InputLength + InputIter,
  Self: Slice<RangeFrom<usize>> + Slice<RangeTo<usize>> + Clone,
{
  type Item = <C as InputIter>::Item;

  fn split_at_position_complete<P, E: ParseError<Self>>(
    &self,
    pred: P,
  ) -> IResult<Self, Self, E>
  where
    P: Fn(Self::Item) -> bool,
  {
    match self.split_at_position(pred) {
      Err(Err::Incomplete(_)) => Ok(self.take_split(self.input_len())),
      res => res,
    }
  }

  fn split_at_position<P, E: ParseError<Self>>(&self, pred: P) -> IResult<Self, Self, E>
  where
    P: Fn(Self::Item) -> bool,
  {
    match self.content.position(pred) {
      Some(n) => Ok(self.take_split(n)),
      None => Err(Err::Incomplete(Needed::new(1))),
    }
  }

  fn split_at_position1<P, E: ParseError<Self>>(
    &self,
    pred: P,
    e: ErrorKind,
  ) -> IResult<Self, Self, E>
  where
    P: Fn(Self::Item) -> bool,
  {
    match self.content.position(pred) {
      Some(0) => Err(Err::Error(E::from_error_kind(self.clone(), e))),
      Some(n) => Ok(self.take_split(n)),
      None => Err(Err::Incomplete(Needed::new(1))),
    }
  }

  fn split_at_position1_complete<P, E: ParseError<Self>>(
    &self,
    pred: P,
    e: ErrorKind,
  ) -> IResult<Self, Self, E>
  where
    P: Fn(Self::Item) -> bool,
  {
    match self.content.position(pred) {
      Some(0) => Err(Err::Error(E::from_error_kind(self.clone(), e))),
      Some(n) => Ok(self.take_split(n)),
      None => {
        if self.content.input_len() == 0 {
          Err(Err::Error(E::from_error_kind(self.clone(), e)))
        } else {
          Ok(self.take_split(self.input_len()))
        }
      }
    }
  }
}

impl<'l, C> Offset for Input<'l, C> {
  fn offset(&self, second: &Self) -> usize {
    second.offset - self.offset
  }
}

impl<'l, F: FromStr, C> ParseTo<F> for Input<'l, C>
where
  C: ParseTo<F>,
{
  fn parse_to(&self) -> Option<F> {
    self.content.parse_to()
  }
}

macro_rules! impl_slice_range {
  ($content_type:ty, $range_type:ty, $can_return_self:expr) => {
    impl<'l> Slice<$range_type> for Input<'l, $content_type> {
      fn slice(&self, range: $range_type) -> Self {
        if $can_return_self(&range) {
          return self.clone();
        }

        let next_content = self.content.slice(range);
        let consumed_len = self.content.offset(&next_content);
        if consumed_len == 0 {
          return Input {
            line: self.line,
            column: self.column,
            offset: self.offset,
            content: next_content,
            filename: self.filename,
          };
        }

        let consumed = self.content.slice(..consumed_len);
        let next_offset = self.offset + consumed_len;

        let consumed_as_bytes = consumed.as_bytes();
        let iter = Memchr::new(b'\n', consumed_as_bytes);
        let number_of_lines = iter.count() as u32;
        let next_line = self.line + (number_of_lines as usize);

        let before_self = &self.get_unoffsetted_slice()[..next_offset];

        let next_column = match memchr::memrchr(b'\n', before_self) {
          None => next_offset + 1,
          Some(pos) => next_offset - pos,
        };


        Input {
          line: next_line,
          column: next_column,
          offset: next_offset,
          content: next_content,
          filename: self.filename,
        }
      }
    }
  };
}

macro_rules! impl_slice_ranges {
  ($content_type:ty) => {
    impl_slice_range!($content_type, Range<usize>, |_| false);
    impl_slice_range!($content_type, RangeTo<usize>, |_| false);
    impl_slice_range!($content_type, RangeFrom<usize>, |range:&RangeFrom<usize>| range.start == 0);
    impl_slice_range!($content_type, RangeFull, |_| true);
  }
}

impl_slice_ranges!(&'l str);
impl_slice_ranges!(&'l [u8]);
