# libmarin_input - Marin Project

### What kind of library is this?

This library is written for the parsers of the Marin Project, but you can also use it in your projects if you see fit. The library is currently in an unstable state, so use it at your own risk.

### Documentation

At the moment, the library has no documentation.

### Including the library in your project

To include the library in your project, add this to the Cargo.toml file:

```
[dependencies]
nom = "6.1.2"
libmarin_input = {
  git = "https://github.com/MarinProject/libmarin_input.git",
  version = "0.1.1",
  branch = "main",
}
```

### How to use library
```
use nom::{
  IResult,
  bytes::complete::{
    tag,
    is_not,
  },
};
use libmarin_input::Input as CustomInput;

type Span<'l> = CustomInput<'l, &'l str>;

fn main() {
  let xml = Span::new("filename.xml", "<element></element>");

  let start_tag: IResult<Span, Span> = tag("<")(xml);
  let (next_span, _start_tag) = start_tag.expect("The element tag must start with \"<\"");
  let el_name: IResult<Span, Span> = is_not(">")(next_span);
  let(next_span, el_name) = el_name.expect("The element tag must end with \">\"");
  let format = format!("></{}>", el_name);
  let close_tag: IResult<Span, Span> = tag(format.as_str())(next_span);
}
```

### License

This project is licensed under the MIT license — see the file for details `LICENSE`.
